import React, { Component } from "react";
import { connect } from "react-redux";
import { PLAY_GAME, WIN } from "./redux/constant/taiXiuConstant";

class KetQua extends Component {

  render() {
    console.log(this.props);
    return (
      <div className=" ">
        <button
          onClick={() => {
            this.props.playGame();
          }}
          style={{ width: 250, height: 150, fontSize: 40 }}
          className="btn btn-success"
        >
          Play Game
        </button>
        <div className="d-flex flex-column py-5">
          <h1 style={{color:`${this.props.result ==WIN ? "green" : "red"}`}} >{this.props.textResult}</h1>
          <h2 className="text-danger m-1">Your Choice: {this.props.chosen}</h2>
          <h2 className="text-success m-1">You win: {this.props.win}</h2>
          <h2 className="text-primary m-1">Total: {this.props.total} </h2>
        </div>
      </div>
    );
  }
}
let mapStateToProps = (state) => ({
  chosen: state.taiXiuReducer.chosen,
  win: state.taiXiuReducer.win,
  total: state.taiXiuReducer.total,
  textResult: state.taiXiuReducer.textResult,
  result: state.taiXiuReducer.result
});
let mapDispatchToProps = (dispatch) => {
  return {
    playGame: () => {
      dispatch({
        type: PLAY_GAME,
      });
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(KetQua);
