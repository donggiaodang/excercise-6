import { PLAY_GAME, WIN, XIU } from "../constant/taiXiuConstant";
import { CHOICE, TAI, LOST } from './../constant/taiXiuConstant';
let initialState = {

  diceArr: [
    {
      img: "./img/imgXucSac/1.png",
      value: 1,
    },
    {
      img: "./img/imgXucSac/1.png",
      value: 1,
    },
    {
      img: "./img/imgXucSac/1.png",
      value: 1,
    },
  ],
  chosen: null,
  win: 0,
  total: 0,
  textResult: null,
  result:null,
  
};
export const taiXiuReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case CHOICE: {
      let clonceDiceArr = [...state.diceArr];
      let sum = 0;
      let newTotal = state.total;
      let newChoice = state.chosen;
      console.log("newTotal: ", newTotal);
      let newWin = state.win;
      console.log("newWin: ", newWin);
      for (let i = 0; i < clonceDiceArr.length; i++) {
        sum += clonceDiceArr[i].value;
      }
      if (payload == TAI) {
        newChoice = payload;
     
      } else if (payload == XIU) {
        newChoice = payload;
      }

      newTotal += 1;
      return { ...state, chosen: payload };
    }
    case PLAY_GAME: {
      let newResult = state.result
      let newTextResult = state.textResult;
      let newDiceArr = state.diceArr.map((item) => {
        let random = Math.floor(Math.random() * 6) + 1;

        return {
          img: `./img/imgXucSac/${random}.png`,
          value: random * 1,
        };
      });
      console.log(newDiceArr);
      let resultTotal = newDiceArr.reduce((tong, sp) => {
        tong = tong + sp.value;
        return tong;
      }, 0);
      console.log(resultTotal);
      if(state.chosen!=null){
        if (resultTotal >= 0 && resultTotal <= 11) {
          if (state.chosen == XIU) {
            state.win++;
            // state.win = state.win + 1
            newResult = WIN
            newTextResult = 'CONGRATULATION! YOU WIN'
          }else{
            newTextResult = 'UNFORTUNATELY! YOU LOST'
            newResult = LOST
          }
        }
        if (resultTotal >= 11 && resultTotal <= 18) {
          if (state.chosen == TAI) {
            state.win++;
            // state.win = state.win + 1
            newTextResult = 'CONGRATULATION! YOU WIN'
            newResult = WIN
            newTextResult = 'UNFORTUNATELY! YOU LOST'
            newResult = LOST
          }
        }
      }else{
        newTextResult = 'PLEASE CHOOSE TAI OR XIU BEFORE PLAY GAME'
      }
     
     let newTotal = state.total;
     newTotal+=1;
    
      return { ...state, diceArr: newDiceArr, total:newTotal, textResult: newTextResult, result: newResult  };
    }

    default:
      return state;
  }
};
