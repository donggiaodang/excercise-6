import React, { Component } from 'react'
import { connect } from 'react-redux'
import { CHOICE, TAI, XIU } from './redux/constant/taiXiuConstant';

 class XucXac extends Component {
  
  renderDiceArray=()=>{
    return this.props.diceArr.map((item)=>{
     return <img style={{width: 100, margin: 10, borderRadius: 8}} src={item.img} alt="123" />
    })
  }
  render() {
    return (
      <div className='d-flex justify-content-between container pt-5'>
        <button onClick={()=>{this.props.handleChoice(TAI)}} style={{width: 150, height:150, fontSize:40}} className='btn btn-danger'>Tai</button>
        <div>{this.renderDiceArray()}</div>
        <button onClick={()=>{this.props.handleChoice(XIU)}} style={{width: 150, height:150, fontSize:40}} className='btn btn-dark'>Xiu</button>
      </div>
    )
  }
}
let mapDispatchToProps = (dispatch)=>{
  return{
    handleChoice:(value)=>{
      dispatch(
        {
          type: CHOICE,
          payload: value
        }
      )
    }
  }
}
let mapStateToProps =(state)=>({
  diceArr: state.taiXiuReducer.diceArr
})
export default connect(mapStateToProps,mapDispatchToProps)(XucXac)